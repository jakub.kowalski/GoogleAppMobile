import React, { Component } from 'react';
import { StyleSheet, View, Alert, Dimensions, KeyboardAvoidingView } from 'react-native';
import MapViewDirections from 'react-native-maps-directions';
import axios from 'react-native-axios';
import MapView, { Marker } from 'react-native-maps';
import { FormLabel, FormInput, Button,Header } from 'react-native-elements';





const { width, height } = Dimensions.get('window');

class route extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            from: '',
            to: '',
            coordinates: [{ latitude: 0, longitude: 0 }, { latitude: 0, longitude: 0 },],
            center: {
                latitude: 53.127547,
                longitude: 14.55750,
                latitudeDelta: 1,
                longitudeDelta: 1,
            }
        }
        this.onClickFind = this.onClickFind.bind(this);

    }


    onClickFind() {
        let startPosition = this.state.from;
        let endPosition = this.state.to;

        axios.get('https://maps.googleapis.com/maps/api/geocode/json', {
            params: {
                address: startPosition,
                key: 'AIzaSyByGxxRtplrnlA7JO3x1Wfu3HEA4l6lv94'
            }
        })
            .then((response) => {
                const startLat = response.data.results[0].geometry.location.lat;
                const startLng = response.data.results[0].geometry.location.lng;
                return axios.get('https://maps.googleapis.com/maps/api/geocode/json', {
                    params: {
                        address: endPosition,
                        key: 'AIzaSyByGxxRtplrnlA7JO3x1Wfu3HEA4l6lv94'
                    }
                })
                    .then((response2) => {
                        this.setState({
                            coordinates: [
                                {
                                    latitude: startLat,
                                    longitude: startLng
                                },
                                {
                                    latitude: response2.data.results[0].geometry.location.lat,
                                    longitude: response2.data.results[0].geometry.location.lng
                                },
                            ],
                            center: { latitude: startLat, longitude: startLng, latitudeDelta: 1.5, longitudeDelta: 2.5 },
                        })
                    })

            })
            .catch(function (error) {
                console.log('Error' + error);
                Alert.alert("Wpisz poprawna nazwe")
            });
    }

    onMapPress = (e) => {
        this.setState({
            coordinates: [
                ...this.state.coordinates,
                e.nativeEvent.coordinate,
            ],
        });
    }


    render() {

        return (
            <View >

                <Header
                    centerComponent={{ text: 'GOOGLE APP', style: { color: '#fff', fontSize: 19 } }}
                    rightComponent={{ icon: 'home', color: '#fff', onPress: () => { this.props.navigation.navigate('Home') }, underlayColor: '#64b5f6' }}
                />

                <FormLabel
                    labelStyle={style.labelStyle} >FROM</FormLabel>
                <FormInput onChangeText={(from) => this.setState({ from })}
                    containerStyle={style.input}
                    inputStyle={{ fontSize: 14 }}
                    underlineColorAndroid='transparent'
                   
                />
                <FormLabel labelStyle={style.labelStyle}>TO</FormLabel>
                <FormInput onChangeText={(to) => this.setState({ to })}
                    labelStyle={style.input}
                    containerStyle={style.input}
                    inputStyle={{ fontSize: 14 }} 
                    underlineColorAndroid='transparent'/>

                <Button
                    buttonStyle={{height:32}}
                    title="SEARCH ROUTE"
                    onPress={this.onClickFind.bind(this)}
                />
                <MapView
                    style={style.map}
                    region={this.state.center}
                    zoom={this.state.zoom}
                    onPress={this.onMapPress}
                >
                    {this.state.coordinates.map((coordinate, index) =>
                        <MapView.Marker key={`coordinate_${index}`} coordinate={coordinate} />
                    )}

                    {(this.state.coordinates.length >= 2) && (

                        <MapViewDirections
                            origin={this.state.coordinates[0]}
                            waypoints={(this.state.coordinates.length > 2) ? this.state.coordinates.slice(1, -1) : null}
                            destination={this.state.coordinates[this.state.coordinates.length - 1]}
                            apikey={'AIzaSyByGxxRtplrnlA7JO3x1Wfu3HEA4l6lv94'}
                            strokeColor="hotpink"
                            strokeWidth={4}
                        />
                    )}
                </MapView>


            </View>
        );
    }


}


const style = StyleSheet.create({
    labelStyle: {
        fontSize: 12,

    },
   
    input: {
        width: 440,
        height: 42,
        borderColor: 'gray',
        marginLeft: 25,
    },

    map: {
        position: 'absolute',
        top: 250,
        left: 0,
        right: 0,
        bottom: 0,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height
    },

})
export default route;