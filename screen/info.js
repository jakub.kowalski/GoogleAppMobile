import React, { Component } from 'react';
import { View, Text, Button, StyleSheet,Dimensions } from 'react-native';
import { Rating } from 'react-native-elements';


class info extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center',backgroundColor: '#fff' }}>
        <Text style={styles.txt}>Project GoogleApi by JakubKowalski </Text>
        <Text style={styles.txt}>Thank you for your visit </Text>
        <Text style={styles.txt}>Version v.1.0.0 </Text>
        <Rating
          showRating
          type="star"
          fractions={1}
          startingValue={4.6}
          imageSize={40}
          onFinishRating={this.ratingCompleted}
          style={{ paddingVertical: 10 }}
        />
        <View style={{ marginTop: 60, width: 300 }}>
          <Button
            title="BACK"
            onPress={() => this.props.navigation.goBack()}
          />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  txt: {
    fontSize: 20,
    marginBottom: 10,
  }

});

export default info;