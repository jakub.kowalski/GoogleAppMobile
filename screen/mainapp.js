
import React from 'react';
import { StyleSheet, Text, View, TextInput, Dimensions, Alert, KeyboardAvoidingView} from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import axios from 'react-native-axios';
import { Header, Button } from 'react-native-elements';
import Icon from "react-native-vector-icons/FontAwesome";
const imageWidth = Dimensions.get("window").width;
class mainapp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            placeholder:'Search',
           
            clearInput:false,
           message: '',
            center: {
                latitude: 53.427547,
                longitude: 14.55787,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421
            },
            marker: {
                title: 'Szczecin',
                latLng: {
                    latitude: 53.42754,
                    longitude: 14.55787
                },
            }, 
        }
        this.handlePress = this.handlePress.bind(this);
    }
    handlePress(event) {
        let loc = this.state.text;

        axios.get('https://maps.googleapis.com/maps/api/geocode/json', {
            params: {
                address: loc,
                key: 'AIzaSyByGxxRtplrnlA7JO3x1Wfu3HEA4l6lv94'
            }
        })
            .then((response) => {

                if (response.data.status === "OK") {
                    const latitude = response.data.results[0].geometry.location.lat;
                    const longitude = response.data.results[0].geometry.location.lng;
                    const latitudeDelta = 0.0922;
                    const longitudeDelta = 0.1421;
                    var title = response.data.results[0].formatted_address;
                    this.setState({
                        center: { latitude, longitude, latitudeDelta, longitudeDelta },
                        marker: { title: title, latLng: { latitude, longitude } },
                    })

                }
            })
            .catch(function (error) {
                console.log(error);
                Alert.alert("Wpisz poprawna nazwe")
            });

    }
 
    render() {

        return (



            <View style={styles.container}>

                <View style={styles.header}>
                    <Header

                        centerComponent={{ text: 'GOOGLE APP', style: { color: '#fff', fontSize: 19 } }}
                        rightComponent={{ icon: 'home', color: '#fff', onPress: () => { this.props.navigation.navigate('Home') }, underlayColor: '#64b5f6' }}

                    />
                </View>

                <MapView
                    style={styles.map}
                    region={this.state.center}
                >
                    <Marker
                        style={styles.specialMarker}
                        coordinate={this.state.marker.latLng}
                        title={this.state.marker.title} >

                    </Marker>
                </MapView>



                <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={65}>
                    <View style={styles.InputBox}>

                        <TextInput
                             style={styles.textinput}                        
                         onChangeText={(text) =>  this.setState({ text })}
                             underlineColorAndroid='transparent'
                           placeholder={this.state.placeholder}
                            
                            
                              
                        />

                        <Button
                            title="Find"
                            onPress={this.handlePress.bind(this)}
                            buttonStyle={{
                                backgroundColor: "rgba(92, 99,216, 1)"

                            }}
                       
                        />
                        <Button
                            icon={{ name: 'direction', type: 'entypo', style: { marginRight: 0 } }}
                            buttonStyle={{
                                backgroundColor: "rgba(92, 99,216, 1)"
                            }}
                            style={styles.directions}
                            onPress={this.SearchRoute.bind(this)}
                        />

                    </View>
                </KeyboardAvoidingView>



            </View>

        );
    }
}
const styles = StyleSheet.create({
    container: {

        justifyContent: 'flex-end',
        flex: 1
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },

    text: {

        backgroundColor: '#fff',

    },
    specialMarker: {
        zIndex: 1,
    },
    textinput: {
        width: 150,
        padding: 5,
        borderColor: '#48BBEC',
        backgroundColor: 'white',


    },
    InputBox: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        paddingVertical: 20,
        paddingHorizontal: 20


    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        zIndex: 2,

    },




});
export default mainapp;