import React, { Component } from 'react';
import { View, Text,StyleSheet, Image,Dimensions } from 'react-native';
import { Header, Button } from 'react-native-elements';
const imageWidth = Dimensions.get("window").width;

class firstscreen extends React.Component {
    render() {
      return (
        <View style={styles.padding}>
          <Image
            source={require('../images/logo.png')}
            style={styles.logo}
          />
          <Text style={styles.txt}  > Project GoogleApi by Jakub Kowalski
            </Text>
          <View style={{ margin: 10, width: 300 }}>
            <View style={{marginBottom: 10 }}>
              <Button
            
               buttonStyle={styles.btn}
                title="START"
                onPress={() => this.props.navigation.navigate('Main')}
              />
            </View>
            <View style={{ marginBottom: 10 }}>
              <Button
      buttonStyle={styles.btn}
                title="FIND ROUTE"
                onPress={() => this.props.navigation.navigate('Directions')}
              />
            </View>
            <View style={{ marginBottom: 10 }}>
              <Button
      buttonStyle={styles.btn}
                title="INFORMATION"
                onPress={() => this.props.navigation.navigate('Information')}
              />
            </View>
            <View style={{ marginBottom: 10 }}>
              <Button
      buttonStyle={styles.btn}
                title="EXIT"
                onPress={() => this.props.navigation.goBack(null)}
              />
            </View>
          </View>
        </View>
      );
    }
  }
  const styles = StyleSheet.create({
    txt: {
      fontSize: 20,
      marginBottom: 30,
    },
   
    padding: {
      flex: 1,
       flexDirection: 'column',
       justifyContent: 'center',
       alignItems: 'center',
      marginBottom: 20,
  
    },
    logo: {
      marginBottom: 65,
    },
  
    btn: {
      alignItems: 'center',
      height: 40,
      elevation: 0,
      backgroundColor: "rgba(92, 99,216, 1)",
      fontSize: 16,
      borderRadius: 10
    }
  });
  
  
  export default firstscreen;
  