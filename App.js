import React from 'react';
import { createStackNavigator } from 'react-navigation';


import firstscreen from './screen/firstscreen';
import info from './screen/info';
import mainapp from './screen/mainapp';
import route from './screen/route';




const RootStack = createStackNavigator(
  {
    Home: {
      screen: firstscreen,
    },
    Main: {
      screen: mainapp,
    },
    Information: {
      screen : info,
    },
    Directions:{
      screen : route,
    }
  },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends React.Component {
  render() {
    return (<RootStack />)
  }
}